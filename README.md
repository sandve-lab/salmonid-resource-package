# Salmonid resource package

R package for retrieve of salmonid and related data resources, including: annotation, expression, sequence and genomic track data.
This git project is for the development and testing of ideas related to the package.

Google doc for planning:
https://docs.google.com/document/d/1dN9Q989IBA2Q7qFCOlIc_CYo9b8OKhsJPxvwOKoSWuQ/edit?usp=sharing
